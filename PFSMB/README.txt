This file is part of the distribution of the Paralel French Social Media
Bank (PFSMB). 

** Contact:
Djamé Seddah (Djame.seddah@inria.fr)


** Description
The PFSMB is a collection of French-English parallel sentences
manually translated from an extension of the French Social Media
Bank (Seddah et al., 2012)[1] which contains texts collected on
Facebook, Twitter, as well as from the forums of JeuxVideos.com and
Doctissimo.fr[2]. This corpus, consists of 1,554 comments in French 
annotated with different kind of linguistic information: Part-of-Speech tags, 
surface syntactic representations, as well as a normalized form whenever necessary.
 Comments have been translated from French to English by a native French speaker
and extremely fluent, near-native, English speaker. Typographic and
grammatical error were corrected in the gold translations but the
language register was kept. For instance, idiomatic expressions
were mapped directly to the corresponding ones in English (e.g.
'mdr' has been translated to 'lol' and letter repetitions were also
kept (e.g. 'ouiii' has been translated to yesss'). For our
experiments, we have divided the Cr#pbank into a test set and a
blind test set containing 777 comments each.

This corpus was built within the French Nationaal Research Agency's Parsiti project
(2016-2022, ANR-16-CE33-0021, PI: D.Seddah)


** Statistics
		#Sent.	#tokens	ASL	TTR
--------------------------------------------
PFSMB Test	777	13,680	17.60	0.32	
PFSMB Blind	777	12,808	16.48	0.37
--------------------------------------------

TTR stands for Type-to-Token Ratio, ASL for average sentence length.
The PFSMB is much more noisy than the MNTN for example (Michel & Neubig,
2019)[3] as shown by this table:

Metrics x Test set	PFSMB	MTNT	Newstests	OpenSubsTest
--------------------------------------------------------------------
3-gram KL div		1.563	0.471	0.406		0.0060
% OOVs			12.63	6.78	3.81		0.76
BPE stab		0.018	0.024	0.049		0.13
Perplexity		559.48	318.24	288.83		62.06
--------------------------------------------------------------------			
Domain-related measure on the source side (FR), between Test sets and 
Large OpenSubtitles training set. Dags indicate UGC corpora.

** Example
fr: si vous me comprenez vivé la mm chose ou avez passé le cap je pren tou ce qui peu m'aider.
en: if you understand me live the same thing or have gotten over it I take everything that can help me.

** Usage 
If you use this corpus for your research, we require that you cite the
following papers:  (Seddah et al., 2012) [1] and (Rosales Nùñez et al.,
2019)[4]
We conceived the original corpus as a crash test for parsing  and this as a
crash test for machine translation systems. Please do not fine-tune, or
train your models on it. The point is to see what MT models can do when
facing real unknown, noisy data. This is why we split it in two : a test set
and a blind test whose results on should only be used for comparison with
other works.   

** LICENCE
As  the originial French Social Media Bank from which the PFMSB is derived,
the licence is CC-BY-NC-SA 4.0.[5]
In short: you can evualate your models on it but you cannot it use as part of
any training/fine-tuning set set that would ultimately be used in a commercial product.

** Contributors
PFSMB: Emilia Verzini and Djamé Seddah
FSMB: Djamé Seddah, Benoit Sagot, Marie Candito, Virginie Mouilleron and
Vanessa Combet

-------------------------------------------------------------------------
	
[1] Djamé Seddah, Benoît Sagot, Marie Candito, Virginie Mouilleron, and Vanessa Combet. 2012. 
The french social media bank: a treebank of noisy user generated content. 
In COLING 2012, 24th International Conference on Computational Linguistics, 
Proceedings of the Conference: Technical Papers, 8-15 December 2012, Mumbai, India, pages 2441–2458.
[2]Popular French websites devoted respectively to videogames and health
issues and advices.
[3] Paul Michel and Graham Neubig. 2018. MTNT: A testbed for machine translation of noisy text. 
In Proceedings of the 2018 Conference on Empirical Methods in Natural Language Processing, Brussels, 
Belgium, October 31 - November 4, 2018, pages 543– 553.
[4] José Carlos Rosales Nunez, Djamé Seddah and Guillaume Wisniewski. 2019. Comparison between NMT and PBSMT Performance for Translating Noisy User-Generated
Content  Proceedings of the 22nd Nordic Conference on Computional Linguistics (NoDaLiDa), September 30-October 2, Turku, Finland
[5] https://creativecommons.org/licenses/by-nc-sa/4.0/
